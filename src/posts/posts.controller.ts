import { 
    Controller, 
    Post, 
    Get, 
    Body, 
    Delete, 
    UseInterceptors, 
    Query, 
    Param,
    Res,
    UploadedFile,
    Put,
    Req,
    UseGuards
} from '@nestjs/common';
import { FileInterceptor } from '@nestjs/platform-express';
import { diskStorage } from "multer";

import { MulterValidators } from '../validators/multer.validator';
import { PostsService } from './posts.service';
import { Posts, CommentType } from './posts.model';
import { AuthGuard } from '../auth/auth.guard';

@Controller('posts')
export class PostsController {

    constructor(private readonly postsService: PostsService){ }


    @Get("get-image/:id")
    getImage(@Param("id") image: string, @Res() res){
        return res.sendFile(image, { root: "uploads" });
    }

    @UseGuards(AuthGuard)
    @Post("add-post-with-image")
    @UseInterceptors(
        FileInterceptor("postImage", {
            storage: diskStorage({
                destination: "./uploads",
                fileName: MulterValidators.editFileName
            }),
            fileFilter: MulterValidators.imageFileFilter
        })
    )
    async addPost(@UploadedFile() postImage, 
                    @Body("postTitle") postTitle: string,
                    @Body("postSnippet") postSnippet: string, 
                    @Body("postBody") postBody: string, 
                    @Body("postCategory") postCategory: string,
                    @Req() req): Promise<Posts> 
    {
        const imagePath: string = `${req.protocol}://${req.get("host")}/posts/get-image/${postImage.filename}`;

        return await this.postsService.addPostWithImage(postTitle, postSnippet, postBody, postCategory, imagePath);
    }

    @UseGuards(AuthGuard)
    @Post("add-post-without-image")
    async addPostWithoutImage(
        @Body("postTitle") postTitle: string,
        @Body("postSnippet") postSnippet: string,
        @Body("postBody") postBody: string,
        @Body("postCategory") postCategory: string
    ): Promise<Posts> {
        return await this.postsService.addPostWithoutImage(postTitle, postSnippet, postBody, postCategory);
    }

    @Post("add-comment-to-post-without-image/:id")
    async addCommentwithoutImage(
        @Param("id") postId: string,
        @Body("comment") comment: string,
        @Body("name") commentName: string,
        @Body("image") commentImage: string,
        @Req() req
    ): Promise<Posts> {
        commentImage = `${req.protocol}://${req.get("host")}${commentImage}`;
        return await this.postsService.addComment(postId, comment, commentName, commentImage);
    }

    @Post("add-comment-to-post-with-image/:id")
    @UseInterceptors(
        FileInterceptor("image", {
            storage: diskStorage({
                destination: "./uploads",
                fileName: MulterValidators.editFileName
            }),
            fileFilter: MulterValidators.imageFileFilter
        })
    )
    async addCommentWithImage(
        @Req() req,
        @Param("id") postId: string,
        @UploadedFile() image,
        @Body("comment") comment: string,
        @Body("name") commentName?: string,
    ): Promise<Posts> {
        const commentImagePath: string = `${req.protocol}://${req.get("host")}/posts/get-image/${image.filename}`;
        return await this.postsService.addComment(postId, comment, commentName, commentImagePath);
    }

    @Get("get-posts-per-category")
    async getPostsPerCategory(
        @Query("id") categoryName: string, 
        @Query("start") start?: number, 
        @Query("end") end?: number
    ): Promise<Posts[]> {
        return await this.postsService.getPostsByCategory(categoryName, start, end);
    }

    @Get("get-all-posts")
    async getPosts(
        @Query("start") start?: number, 
        @Query("end") end?: number
    ): Promise<Posts[]> {
        return await this.postsService.getPosts(start, end);
    }

    @Get("get-post/:id")
    async getPost(@Param("id") postId: string): Promise<Posts> {
        return await this.postsService.getPost(postId);
    }

    @UseGuards(AuthGuard)
    @Get("date-of-last-post")
    async getDateOfLastPost(): Promise<Date> {//summary
        return await this.postsService.getDateOfLastPost();
    }

    @UseGuards(AuthGuard)
    @Get("count-all-comments")
    async countAllComments(): Promise<number> {//summary
        return await this.postsService.countAllComments();
    }

    @UseGuards(AuthGuard)
    @Delete("delete-post/:id")
    async deletePost(@Param("id") postId): Promise<{ message: string }> {
        return this.postsService.deletePost(postId);
    }

    @UseGuards(AuthGuard)
    @Put("edit-post-without-image/:id")
    async editPost(
        @Param("id") postId: string,
        @Body("postImage") postImage: string,
        @Body("postTitle") postTitle: string,
        @Body("postSnippet") postSnippet: string,
        @Body("postBody") postBody: string,
        @Body("postCategory") postCategory: string
        ): Promise<Posts | any> {
            return await this.postsService.editPost(postId, postTitle, postBody, postSnippet, postCategory, postImage);
    }

    @UseGuards(AuthGuard)
    @Put("edit-post-with-image/:id")
    @UseInterceptors(
        FileInterceptor("postImage", {
            storage: diskStorage({
                destination: "./uploads",
                fileName: MulterValidators.editFileName
            }),
            fileFilter: MulterValidators.imageFileFilter
        })
    )
    async editPostWithImage(
        @UploadedFile() postImage,
        @Param("id") postId,
        @Body("postTitle") postTitle,
        @Body("postSnippet") postSnippet: string,
        @Body("postBody") postBody: string,
        @Body("postCategory") postCategory: string,
        @Req() req
    ): Promise<Posts> {
        //try deleting the previous image
        const postImagePath = `${req.protocol}://${req.get("host")}/posts/get-image/${postImage.filename}`;
        return await this.postsService.editPost(postId, postTitle, postBody, postSnippet, postCategory, postImagePath);
    }

    //matches: /posts/search-for-posts?searchFor=text
    @Get("search-for-posts")
    async seachForPosts(@Query("searchFor") searchFor: string): Promise<Posts[]> {
        return await this.postsService.searchforPosts(searchFor);
    }

    @Get("comments-per-post/:id")
    async getCommentsPerPost(@Param(":id") postId: string): Promise<CommentType[]> {
        return await this.postsService.getCommentsPerPost(postId);
    }

    @Get("count-all-posts")
    async countAllPosts(): Promise<number> {
        return await this.postsService.countTotalPosts();
    }

    @Get("count-posts-per-category/:id")
    async countPostsPerCategory(@Param("id") categoryName: string): Promise<{ total: number }> {
        return await this.postsService.countPostsPerCategory(categoryName);
    }

    @Get("count-comments-per-post/:id")
    async countCommentsPerPost(@Param("id") postId: string): Promise<{ total: number }> {
        return await this.postsService.countCommentsPerPost(postId);    
    }
}