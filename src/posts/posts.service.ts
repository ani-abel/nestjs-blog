import { Injectable, ForbiddenException } from '@nestjs/common';
import { InjectModel } from '@nestjs/Mongoose';
import { Model } from "mongoose";

import { Posts, CommentType } from './posts.model';
import { PostCategoryService } from '../post-category/post-category.service';

@Injectable()
export class PostsService {

    constructor(@InjectModel("PostSchema") private readonly postModel: Model<Posts>, 
    private postCategoryService: PostCategoryService){ }

    async addPostWithImage(        
        postTitle: string, 
        postSnippet: string, 
        postBody: string, 
        postCategory: string,
        postImage?: string
        ): Promise<Posts>{
        if(await this.postExists(postTitle)){
            throw new ForbiddenException("Similar post already exists");
        }
        //validate length of snippet against 30 words
        if(this.postSnippetExceedsLength(postSnippet)){
            throw new ForbiddenException("Snippet cannot > 30 words");
        }

        const newPost = new this.postModel({ postImage, postTitle, postSnippet, postBody, postCategory, comments: [] });

        return await newPost.save() as Posts;
    }
    
    async addPostWithoutImage(
        postTitle: string, 
        postSnippet: string, 
        postBody: string, 
        postCategory: string,
    ): Promise<Posts> {
        //validate length of snippet against 30 words
        if(await this.postSnippetExceedsLength(postSnippet)){
            throw new ForbiddenException("Snippet cannot > 30 words");
        }

        const newPost = new this.postModel({ postTitle, postSnippet, postBody, postCategory, comments: [] });

        return await newPost.save() as Posts;
    }

    async getPost(postId: string): Promise<Posts> {
        try{
            return await this.postModel
                            .findById(postId)
                            .exec() as Posts;
        }
        catch(ForbiddenException){
            throw new ForbiddenException("Getting post failed");
        }
    }

    async getPosts(start: number = 0, end: number = 10): Promise<Posts[]> {
        try{
            return await this.postModel.find({})
                                        .populate({
                                            path: "postCategory",
                                            select: "category -_id"//include field: 'category', exclude field: '_id'
                                        })
                                        .skip(start)
                                        .limit(end)
                                        .sort({ dateCreated: "desc" })
                                        .exec() as Posts[];
        }
        catch(ForbiddenException){
            throw new ForbiddenException("Getting posts failed");
        }
    }

    async getPostsByCategory(postCategory: string, start?: number, end?: number): Promise<Posts[] | any> {
        try{
            //get the post category that matches
            const pc = await this.postCategoryService.findPostCategory(postCategory);

            if(start && end){
                console.log(`Start: ${start}`);
                console.log(`End: ${end}`);
                //Math.max(0, end * (start - 1))
                return await this.postModel
                                .find({ postCategory: pc._id })
                                .skip((start - 1) * end)
                                .limit(end)
                                .sort({ dateCreated: "desc" })
                                .exec() as Posts[];
            }

            return await this.postModel
                            .find({ postCategory: pc._id })
                            .sort({ dateCreated: "desc" })
                            .exec() as Posts[];
        }
        catch(ForbiddenException){
            throw new ForbiddenException("Getting post failed");
        }
    }

    // async getPostsByCategory(postCategory: string, start: number = 0, end: number = 10): Promise<Posts[] | any> {
    //     try{
    //         console.log(`Start service: ${start}`);
    //         console.log(`End Service: ${end}`);
    //         let posts: Posts[] = await this.postModel
    //                                 .find({ })
    //                                 //.where("postCategory").ne([])//where 'postCategory != null'
    //                                 //.where("postCategory").equals(postCategory)                           
    //                                 .populate({
    //                                     path: "postCategory",
    //                                     match: { category: postCategory },
    //                                     select: "category -_id",
    //                                 })
    //                                 .skip(start) 
    //                                 .limit(end) 
    //                                 .sort({ dateCreated: "desc" })                            
    //                                 .exec() as Posts[];
            
    //         return posts.filter(post => post.postCategory !== null);
    //     }
    //     catch(ForbiddenException){
    //         throw new ForbiddenException("Getting posts failed");
    //     }
    // }

    async countPostsPerCategory(postCategory: string): Promise<{ total: number }> {
        try{                             
                let posts: Posts[] = await this.postModel
                                                .find({ })
                                                .populate({
                                                    path: "postCategory",
                                                    match: { category: postCategory },
                                                    select: "category -_id"
                                                })
                                                //.countDocuments()
                                                .exec();

                const total: number = posts.filter(post => post.postCategory !== null).length;
            
                return { total };

                //NOT THE MOST OPTIMAL METHOD, CAN'T KEEP PULLING DATA AND FILTERING ON THE SERVER.
                //TODO: USE 'WHERE' CLAUSE ON .POULATE
        }
        catch(ForbiddenException){
            throw new ForbiddenException("Count failed");
        }
    }

    async countTotalPosts(): Promise<number> {//summary
        try{
            const total: number = await this.postModel
                                            .countDocuments({ })
                                            .exec();
            return total;
        }
        catch(ForbiddenException){
            throw new ForbiddenException("Count failed");
        }
    }

    async countCommentsPerPost(postId: string): Promise<{ total: number }> {
        try{
            const post: Posts = await this.getPost(postId);
            //count number of comments in the node
            const total: number = post.comments.length;
            return { total };
        }
        catch(ForbiddenException){
            throw new ForbiddenException("Count failed");
        }
    }

    async deletePost(postId: string): Promise<{ message: string }> {
        try{
            await this.postModel.deleteOne({ _id: postId }).exec();
            return { message: "Post deleted" };
        }
        catch(ForbiddenException){
            throw new ForbiddenException("Delete failed");
        }   
    }

    async editPost(
        postId: string, 
        postTitle: string, 
        postBody: string, 
        postSnippet: string, 
        postCategory: string,
        postImage?: string
    ): Promise<Posts> {
        //find the given post with this id
        const post: Posts = await this.getPost(postId);
        
        if(post.postTitle !== postTitle){
            post.postTitle = postTitle;
        }
        if(post.postBody !== postBody){
            post.postBody = postBody;
        }
        if(post.postSnippet !== postSnippet){
            post.postSnippet = postSnippet;
        }
        if(post.postImage && post.postImage !== postImage){
            post.postImage = postImage;
        }
        if(post.postCategory !== postCategory){
            post.postCategory = postCategory;
        }

        return await post.save() as Posts;
    }

    async addComment(postId: string, commentMessage: string, commentName?: string, commentImage?: string): Promise<Posts> { 
        //get the post
        try{
            const post: Posts = await this.getPost(postId);

            const comment: CommentType = { comment: commentMessage, name: commentName, imagePath: commentImage };

            post.comments.push(comment);

            return await post.save() as Posts;
        }
        catch(NotFoundException){
            throw new NotFoundException("This post was not found");
        }
    }

    async getCommentsPerPost(postId: string): Promise<CommentType[]> {
        try{
            const post: Posts = await this.getPost(postId);
            return (post.comments as CommentType[]);//cast result
        }
        catch(ForbiddenException){
            throw new ForbiddenException("Getting comments failed");
        }
    }

    async searchforPosts(searchText: string): Promise<Posts[]> {
        return await this.postModel
                        .find(
                            {$or: [
                                { postTitle: 
                                    { 
                                        '$regex': searchText, 
                                        '$options': 'i' 
                                    } 
                                },
                                { postSnippet:
                                    { 
                                        '$regex': searchText, 
                                        '$options': 'i' 
                                    } 
                                }]
                            })
                        .exec();
    }

    async getDateOfLastPost(): Promise<Date> {//summary
     const post = await this.postModel.findOne({})
                                    .sort({ dateCreated: "desc" })
                                    .limit(1)
                                    .exec();

     return post.dateCreated;
    }

    async countAllComments(): Promise<number> {
        let total: number = 0;
        const allPosts = await this.postModel
                                    .find({})
                                    .exec();
        
        if(allPosts.length > 0){
            total = allPosts.reduce((total: number, post) => total + post.comments.length, 0);
        }    
        return total;                       
    }

    private async postExists(postTitle: string): Promise<boolean> {
        return await this.postModel.exists({ postTitle });
    }

    //returns false if the number of words is > 30
    private postSnippetExceedsLength(postSnippet: string) {   
        const snippets: string[] = postSnippet.split(" ");
        return (snippets.length >= 30);
    }
}