import * as Mongoose from "mongoose";

import * as MongoosePaginate from "mongoose-paginate";

export const PostSchema = new Mongoose.Schema({
    postImage: { type: String, required: true, default: "/posts/get-image/background-black-colors-952670.jpg" },
    postTitle: { type: String, required: true },
    postSnippet: { type: String, required: true },
    postBody: { type: String, required: true },
    postCategory: { type: Mongoose.Schema.Types.ObjectId, ref: "PostCategorySchema" },
    dateCreated:  { type: Date, required: true, default: Date.now },
    comments: [
        { 
            imagePath: { type: String, required: true, default: "/uploads/user_male.png" },
            name: { type: String, required: true, default: "anonymous" },
            dateCreated: { type: Date, required: true, default: Date.now },
            comment: {  type: String, required: true }            
        }
    ]
});

PostSchema.plugin(MongoosePaginate);

export type CommentType = { imagePath?: string, name?: string, dateCreated?: Date, comment: string };

export interface Posts extends Mongoose.Document {
    _id: string;
    postImage: string;
    postTitle: string;
    postBody: string;
    postSnippet: string;
    postCategory?: string;
    dateCreated: Date;
    comments? : CommentType[];
}