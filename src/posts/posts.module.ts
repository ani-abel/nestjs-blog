import { Module } from '@nestjs/common';
import { MongooseModule } from "@nestjs/mongoose";

import { PostSchema } from './posts.model';
import { PostsController } from './posts.controller';
import { PostsService } from './posts.service';
import { PostCategoryModule } from '../post-category/post-category.module';

@Module({
  imports: [
    MongooseModule.forFeature([{ name: "PostSchema", schema: PostSchema }]),
    PostCategoryModule
  ],
  controllers: [PostsController],
  providers: [PostsService]
})
export class PostsModule {}