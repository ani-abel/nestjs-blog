import { Injectable, HttpException, HttpStatus } from "@nestjs/common";
import { PassportStrategy } from "@nestjs/passport";
import { Strategy, ExtractJwt, VerifiedCallback } from "passport-jwt";
import { AuthService } from "./auth.service";

@Injectable()
export class JwtStrategy extends PassportStrategy(Strategy){
    //init PassportStrategy
    constructor(private readonly authService: AuthService){
        super({
            jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
            secretOrKey: "secretKey" 
        });
    }

    async validate(payload: any, done: VerifiedCallback): Promise<any> {
        const user = await this.authService.validateUser(payload);
        if(!user){
            //throw exception
            return done(
                new HttpException("Unauthorized access", HttpStatus.UNAUTHORIZED),
                false
            );
        }
        return done(null, user, payload.lat);
    }
}