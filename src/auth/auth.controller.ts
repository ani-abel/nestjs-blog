import { Controller, Post, Body } from "@nestjs/common";
import { AuthService } from "./auth.service";
import { UsersService } from "../users/users.service";

@Controller("auth")
export class AuthController {
    constructor(private readonly authService: AuthService, private readonly userService: UsersService){ }

    @Post("login")
    async login(@Body("username") username: string, @Body("password") password: string): Promise<any> {
        const user = await this.userService.getUser(username, password);
        //construct a payload for the user, using username & dateCreated
        const payload: { _id: string, username: string, dateCreated: Date } = {
            _id: user._id,
            username: user.username,
            dateCreated: user.dateCreated
        };

        //create the token
        const token = await this.authService.signPayload(payload);
        return { 
            user: 
            { //carry out this mapping of the user object to avoid putting the password into the server's response
                _id: user.id, 
                username: user.username, 
                imagePath: user.imagePath,
                dateCreated: user.dateCreated
            },
            token, 
            expiresIn: this.authService.getTokenExpiresIn() 
        };
    }

    @Post("register")
    async register(@Body("username") username: string, @Body("password") password: string): Promise<any> {
        const user = await this.userService.addUser(username, password);

        const payload: { username: string, dateCreated: Date } = {
            username: user.username,
            dateCreated: user.dateCreated
        };

        const token = await this.authService.signPayload(payload);
        return { user, token };
    }
}