import { CanActivate, ExecutionContext, Injectable, NotFoundException } from "@nestjs/common";
import { Observable } from "rxjs";
import { decode } from "jsonwebtoken";

@Injectable()
export class AuthGuard implements CanActivate {

    canActivate(context: ExecutionContext): boolean | Observable<boolean> | Promise<boolean> {
        try{
            const request = context.switchToHttp().getRequest();
            return this.validateRequest(request);
        }
        catch(NotFoundException){
            throw new NotFoundException("Token is invalid...or token has expired");
        } 
    }

    private validateRequest(request: any): boolean | Observable<boolean> | Promise<boolean> | any {
       let returnValue: boolean = false;

       if(request.headers.authorization){
           const rawToken: string = (request.headers.authorization as string).split(" ")[1];//splits the token into "Bearer: token"
           //decode the token to use it's details
           const decodedToken: any = decode(rawToken);

           if(decodedToken){
              //send the data decoded data from the token to the request object so any route using this AuthGuard can get access to the user's data
              request.userData = { 
                  _id: decodedToken._id, 
                  username: decodedToken.username, 
                  dateCreated: decodedToken.dateCreated 
                };

              returnValue = true;
           }
       }
        else {
            throw new NotFoundException("Token is invalid...or token has expired");
        }
        return returnValue;
    }
}