import { Injectable } from "@nestjs/common";
import { sign } from "jsonwebtoken";
import { UsersService } from "../users/users.service";

@Injectable()
export class AuthService {
    private tokenExpiresIn: number;

    constructor(private readonly userService: UsersService){ }

    async signPayload(payload: any) {
        this.tokenExpiresIn = 86400;//24 hours in seconds
        return sign(payload, "secretkey", { expiresIn: "24h" });//all signed payloads expire in 7days
    }

    async validateUser(payload: any){
        return await this.userService.getUserByPayload(payload);
    }

    setTokenExpiresIn(expiresIn: number): void {
        this.tokenExpiresIn = expiresIn;
    }

    getTokenExpiresIn(): number {
        return this.tokenExpiresIn;
    }
}