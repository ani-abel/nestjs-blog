import * as Mongoose from "mongoose";
import * as UniqueValidator from "mongoose-unique-validator";

export const ContactSchema = new Mongoose.Schema({
    name: { type: String, required: true },
    phone: { type: String, required: true, unique: true },
    email: { type: String , unique: true },
    message: { type: String, required: true },
    dateCreated: { type: Date, required: true, default: Date.now }
});

//pull in the unique validator schema into this model
ContactSchema.plugin(UniqueValidator);

export interface Contact extends Mongoose.Document {
    _id: string;
    name: string;
    phone: string;
    email?: string;
    message: string;
    dateCreated: Date;
}