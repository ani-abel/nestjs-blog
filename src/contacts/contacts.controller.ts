import { Controller, Post, Get, Body, Param, UseGuards, Query } from '@nestjs/common';
import { ContactsService } from './contacts.service';
import { Contact } from './contact.model';
import { AuthGuard } from '../auth/auth.guard';

@Controller('contacts')
export class ContactsController {

    constructor(private readonly contactService: ContactsService){ }

    @Post("add-contact-message")
    async addContactMessage(
        @Body("name") name: string, 
        @Body("message") message, 
        @Body("phone") phoneNumber: string, 
        @Body("email") emailAddress?: string): Promise<Contact> {   
        return await this.contactService.addContactMessage(name, phoneNumber, message, emailAddress);
    }

    @UseGuards(AuthGuard)
    @Get("get-contact-messages")
    async getContactMessages(@Query("start") start?: number, @Query("end") end?: number): Promise<Contact[]> {
        return await this.contactService.getContactMessages(start, end);
    }

    @UseGuards(AuthGuard)
    @Get("get-contact-message/:id")
    async getContactMessage(@Param("id") contactId: string): Promise<Contact> {
        return await this.contactService.getContactMessage(contactId);
    }

    @UseGuards(AuthGuard)
    @Get("count-all-messages")
    async countAllMessages(): Promise<number> {//summary
        return await this.contactService.countMessages();
    }
}