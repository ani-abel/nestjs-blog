import { Injectable, ForbiddenException } from '@nestjs/common';
import { InjectModel } from "@nestjs/mongoose";
import { Model } from 'mongoose';

import { Contact } from './contact.model';

@Injectable()
export class ContactsService {

    constructor(@InjectModel("ContactSchema") private readonly contactModel: Model<Contact>){ }

    async addContactMessage(name: string, phone: string, message: string, email?: string,): Promise<Contact> {
        //add the message to the mongoose document
        const newMessage = new this.contactModel({ name, email, phone, message });//create a mongoose record based on the given model
        if(await this.verifyIfMessageExists(phone, email)){
            throw new ForbiddenException("This email and phone number are already in use");
        }  
        return await newMessage.save();
    }//returns a copy of the object that was saved to mongodb

    async getContactMessages(start: number = 0, end: number = 10): Promise<Contact[]> {
        try{
            return await this.contactModel
                            .find({})
                            .skip(start)
                            .limit(end)
                            .sort({ dateCreated: "desc" })
                            .exec() as Contact[];
        }
        catch(ForbiddenException){
            throw new ForbiddenException("Getting messages failed");
        }
    }

    //bonus: route
    async getContactMessage(messageId: string): Promise<Contact> {
        try{
            return await this.contactModel
                            .findOne({ _id: messageId })
                            .exec();
        }
        catch(ForbiddenException) {
            throw new ForbiddenException("Getting Message failed");
        }
    }

    async countMessages(): Promise<number> {
        const total = await this.contactModel
                                .find({})
                                .countDocuments()
                                .exec();
        return total;
    }

    //method to check if a email and phone are not unique
    private async verifyIfMessageExists(phone: string, email?: string): Promise<boolean> {
        return await (email ? this.contactModel.exists({ email, phone }) :  this.contactModel.exists({ phone }));
    }

}