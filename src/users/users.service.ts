import { Injectable, ForbiddenException } from '@nestjs/common';
import { InjectModel } from "@nestjs/mongoose";
import { Model } from "mongoose";
import { User } from './users.model';

@Injectable()
export class UsersService {

    constructor(@InjectModel("UserSchema") private readonly userModel: Model<User>) { }

    async getUser(username: string, password: string): Promise<User> {
       try{
            return await this.userModel
                            .findOne({ username, password })
                            .exec();
       }
       catch(ForbiddenException){
           throw new ForbiddenException("Getting this user failed");
       }
    }

    async getUsers(): Promise<User[]> {
        try{
            return await this.userModel
                            .find({})
                            .exec();
        }
        catch(ForbiddenException){
            throw new ForbiddenException("Getting user failed");
        }
    }

    async addUser(username: string, password: string): Promise<User> {
        //create a new user
        const newUser = new this.userModel({ username, password });
        const userExists = await this.userExists(username, password);
        if(userExists.exists){
            throw new ForbiddenException(`${username} already exists`);
        }
        if(password.length > 7){
            throw new ForbiddenException("Password is > 7");
        }
        if(password.length < 5){
            throw new ForbiddenException("Password is < 5");
        }
        return await newUser.save();
    }

    async userExists(username: string, password: string): Promise<{ exists: boolean }> {
        const userExists: boolean = await this.userModel.exists({ username, password });
        return { exists: userExists };
    }

    async countUsers(): Promise<number> {//summary
        const total: number = await this.userModel.countDocuments({});
        return total;
    }

    async getUserById(userId: string): Promise<User | any> {
        try{
            const user: User = await this.userModel
                                         .findOne({ _id: userId })
                                         .exec();

            return { //map password out for security reasons
                _id: user._id, 
                username: user.username, 
                dateCreated: user.dateCreated, 
                imagePath: user.imagePath 
            };
        }
        catch(ForbiddenException) {
            throw new ForbiddenException("Getting user failed");
        }
    }

    async getUserByPayload(payload: any): Promise<User> {
        const { username } = payload;
        return await this.userModel.findOne({ username });
    }
}