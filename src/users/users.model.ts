import * as Mongoose from "mongoose";
import * as UniqueValidator from "mongoose-unique-validator";

export const UserSchema = new Mongoose.Schema({
   imagePath: { type: String, default: "/posts/get-image/user_male.png" },
   username: { type: String, required: true, unique: true },
   password: 
   { 
       type: String, 
       required: true, 
       unique: true, 
       minlength: 5, 
       maxlength: 7
    },
   dateCreated: { type: Date, required: true, default: Date.now }
});

// var userSchema = new Schema({
//     phone: {
//       type: String,
//       validate: {
//         validator: function(v) {
//           return /\d{3}-\d{3}-\d{4}/.test(v);
//         },
//         message: props => `${props.value} is not a valid phone number!`
//       },
//       required: [true, 'User phone number required']
//     }
//   });

//inject the default validator into the model
UserSchema.plugin(UniqueValidator);

export interface User extends Mongoose.Document {
    _id: string;//stands for '_id: ObjectId()' from mongoDb
    imagePath: string;
    username: string;
    password: string;
    dateCreated: Date;
}