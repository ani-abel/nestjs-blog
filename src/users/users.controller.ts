import { Controller, Query, Get, Post, Body, Param, Req, UseGuards } from '@nestjs/common';
import { UsersService } from './users.service';
import { User } from './users.model';
import { AuthGuard } from '../auth/auth.guard';

//@UseGuards(): attach make sure the user is valid and has a token before allowing them through this route
@UseGuards(AuthGuard)
@Controller('users')
export class UsersController {

    constructor(private readonly usersService: UsersService){ }

    @Get("get-user")
    async getUser(@Query("username") username: string, @Query("password") password: string, @Req() req): Promise<User> {
        const userData: any = req.userData;//gotten from the authGuard
        console.log(userData);
        return await this.usersService.getUser(username, password);
    }

    @Get("get-users")
    async getUsers(@Req() req): Promise<User[]> {
        const userData: any = req.userData;//gotten from the authGuard
        console.log(userData);
        let users: User[] = await this.usersService.getUsers();
        users.forEach((user: User) => user.imagePath = `${req.protocol}://${req.get("host")}${user.imagePath}`);
        return users as User[];
    }
    
    @Get("count-users")
    async countUsers(): Promise<number> {//summary
        return await this.usersService.countUsers();
    }

    @Get("does-user-exist")
    async doesUserExist(@Query("username") username: string, @Query("password") password: string): Promise<{ exists: boolean }> {
        return await this.usersService.userExists(username, password);
    }

    @Get("get-user-by-id/:id")
    async getUserById(@Param("id") userId: string, @Req() req): Promise<User | any> {
        console.log(req.userData);//userData come from the decoded jwt userToken gotten from the AuthGuard

        let user: User = await this.usersService.getUserById(userId);
        user.imagePath = `${req.protocol}://${req.get("host")}${user.imagePath}`;
        return user;
    }

    @Post("add-user")
    async addUser(@Body("username") username: string, @Body("password") password: string): Promise<User> {
        return await this.usersService.addUser(username, password);
    }
}