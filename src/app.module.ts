import { Module } from '@nestjs/common';
import { MongooseModule } from "@nestjs/Mongoose";//import mongodb into the module
import { MulterModule } from "@nestjs/platform-express";//used for multer module

import { AppController } from './app.controller';
import { AppService } from './app.service';
import { UsersModule } from './users/users.module';
import { ContactsModule } from './contacts/contacts.module';
import { PostCategoryModule } from './post-category/post-category.module';
import { PostsModule } from './posts/posts.module';
import { AuthModule } from './auth/auth.module';

@Module({
  imports: [   
    MongooseModule.forRoot(
      "mongodb://localhost:27017/blogDatabase", 
      { 
        useNewUrlParser: true, 
        useCreateIndex: true//added to new version of mongoose 
      }),
      MulterModule.register({
        dest: "./uploads"
      }),
      UsersModule,
      ContactsModule,
      PostCategoryModule,
      PostsModule,
      AuthModule
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}