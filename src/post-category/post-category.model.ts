import * as Mongoose from "mongoose";
import * as UniqueValidator from "mongoose-unique-validator";

export const PostCategorySchema = new Mongoose.Schema({
    category: { type: String, required: true, unique: true },
    dateCreated: { type: Date, required: true, default: Date.now }
});

//inject unique validator
PostCategorySchema.plugin(UniqueValidator);

export interface PostCategory extends Mongoose.Document {
    _id: string;
    category: string;
    dateCreated: Date;
}