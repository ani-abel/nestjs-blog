import { Injectable, ForbiddenException } from '@nestjs/common';
import { InjectModel } from '@nestjs/Mongoose';
import { Model } from 'mongoose';
import { PostCategory } from './post-category.model';

@Injectable()
export class PostCategoryService {

    constructor(@InjectModel("PostCategorySchema") private readonly postCategoryModel: Model<PostCategory>){ }

    async addPostCategory(category: string): Promise<PostCategory> {
        const newCategory = new this.postCategoryModel({ category });
       if(await this.CategoryExists(category)){
           throw new ForbiddenException(`${category} already exists`);
       }
       return await newCategory.save();
    }

    async getPostCategory(categoryId: string): Promise<PostCategory> {
        try{
            return await this.postCategoryModel
                            .findOne({ _id: categoryId })
                            .exec();
        }
        catch(ForbiddenException){
            throw new ForbiddenException("Getting category failed");
        }
    }

    async getPostCategories(): Promise<PostCategory[]> {
        try{
            return await this.postCategoryModel
                            .find({})
                            .sort({ dateCreated: "desc" })
                            .exec();
        }
        catch(ForbiddenException){
            throw new ForbiddenException("Getting categories failed");
        }
    }

    async countTotalPostCategories(): Promise<number> {
        const total = await this.postCategoryModel
                                .countDocuments()
                                .exec();
        return total;
    }

    async findPostCategory(postCategory: string): Promise<PostCategory> {
        try{
            return await this.postCategoryModel
                            .findOne({ category: postCategory })
                            .exec();
        }
        catch(ForbiddenException){
            throw new ForbiddenException("Could not fetch category");
        }
    }

    private async CategoryExists(category: string): Promise<boolean> {
        return await this.postCategoryModel.exists({ category });
    }

}