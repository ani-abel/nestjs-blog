import { Module } from '@nestjs/common';
import { MongooseModule } from "@nestjs/mongoose";

import { PostCategoryController } from './post-category.controller';
import { PostCategoryService } from './post-category.service';
import { PostCategorySchema } from './post-category.model';

@Module({
    imports: [
        MongooseModule.forFeature([{ name: "PostCategorySchema", schema: PostCategorySchema }])
    ],
    controllers: [PostCategoryController],
    providers: [PostCategoryService],
    exports: [PostCategoryService]
})
export class PostCategoryModule { }