import { Controller, Get, Param, Post, Body, UseGuards } from '@nestjs/common';
import { PostCategoryService } from './post-category.service';
import { PostCategory } from './post-category.model';
import { AuthGuard } from '../auth/auth.guard';

@Controller('post-category')
export class PostCategoryController {

    constructor(private readonly postCategoryService: PostCategoryService){ }

    @Get("get-post-categories")//multiple post categories
    async getAllPostCategories(): Promise<PostCategory[]>{
        return await this.postCategoryService.getPostCategories();
    }

    @Get("get-post-category/:id")//single post category
    async getCategory(@Param("id") postCategoryId: string): Promise<PostCategory>{
        return await this.postCategoryService.getPostCategory(postCategoryId);
    }

    @UseGuards(AuthGuard)
    @Get("count-all-post-categories")
    async countAllPostCategories(): Promise<number> {//summary
        return await this.postCategoryService.countTotalPostCategories();
    }

    @UseGuards(AuthGuard)
    @Post("add-post-category")
    async addCategory(@Body("category") categoryName: string): Promise<PostCategory>{
        return await this.postCategoryService.addPostCategory(categoryName);
    }

}